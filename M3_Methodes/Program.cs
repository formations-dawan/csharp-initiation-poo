﻿using System;

class Program
{
    static void Main(string[] args)
    {
        int[] tab1 = { 10, 0, 20, 15 };
        int a1 = 10;
        int b1 = 20;

        #region Questions
        /* Créez les méthodes dans un fichier séparé, et appelez les méthodes correspondantes aux questions ici */
        /*1. Créez moi une méthode "Somme" qui retourne la somme de 2 entiers */
        var monResultat = M3_Methodes.MethodesMagalie.MaSomme(a1, 2);
         Console.WriteLine(monResultat);


        /* 2. Créez moi 2 méthodes "Afficher" (surcharge) :
         * a. Affiche un texte
         * b. Affiche les éléments de tab1
         */

        /*a*/
        M3_Methodes.MethodesMagalie.MonAffichTxt("J'affiche un texte");


        /*b*/        
        M3_Methodes.MethodesMagalie.MonAffichTxt(tab1);


        // 3. Créez moi une surcharge de la méthode "Somme" qui retourne la somme des éléments de tab1
        M3_Methodes.MethodesMagalie.MaSomme(tab1);

        // 4. Créez moi une méthode "Min" qui retourne l'élément le plus petit de tab1
        Console.WriteLine(M3_Methodes.MethodesMagalie.MonMin(tab1));

        // 5. Créez moi une méthode "Moy" qui retourne la moyenne de tab1 (double)
        var moyenne = M3_Methodes.MethodesMagalie.MaMoy(tab1);
        Console.WriteLine(moyenne);

        /* 6. Créez moi une méthode "Permuter" qui permute les valeurs de a1 et b1
         * Exemple : a1 = 20 et b1 = 10
         */
        M3_Methodes.MethodesMagalie.MonInverse(ref a1, ref b1);
        Console.WriteLine(a1);
        Console.WriteLine(b1);
        /* 7. Créez moi une méthode "Calculer" qui prend 2 entiers en entrée, et qui calcule 2 valeurs : la somme et la moyenne.
         * Ces valeurs devront être passées via des paramètres en sortie
         */
        int s;
        double moy;
        M3_Methodes.MethodesMagalie.Calculer(15, 26, out s, out moy, out int min);
        
        #endregion
        #region Reponses
        #endregion
    }
}
