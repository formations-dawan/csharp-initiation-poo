﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M3_Methodes
{
    class MethodesMagalie
    {
        public static int MaSomme(int nb1, int nb2)
        {
            return nb1 + nb2;
        }

        public static void MonAffichTxt(string s1)
        {
            Console.WriteLine(s1);
        }

        public static void MonAffichTxt(int[] montab1)
        {
            foreach (var item in montab1)
            {
                Console.WriteLine(item);
            }
        }

        public static int MaSomme(int[] montab1)
        {
            int resultatT = 0;

            for (int i = 0; i < montab1.Length; i++)
            {
                resultatT += montab1[i]; // resultatT = resultatT + montab1[i] (élement du tableau à l'indice i)
            }

            return resultatT;
        }

        public static int MonMin(int[] montab1)
        {
            int monMin = montab1[0];
            for (int i = 1; i < montab1.Length; i++)
            {
                if (montab1[i] < monMin)
                {
                    monMin = montab1[i];
                }
            }
            return monMin;
        }

        public static double MaMoy(int[] montab1)
        {
            double nbitem = montab1.Length;
            var somme = MaSomme(montab1);

            return somme / nbitem;
        }

        public static void MonInverse(ref int a1, ref int b1)
        {
            int stockA = a1;

            a1 = b1;
            b1 = stockA;
        }

        public static double MaMoy(int nb1, int nb2)
        {
            double nbitem = 2;
            var somme = MaSomme(nb1, nb2);

            return somme / nbitem;
        }

        public static void Calculer(int nb1, int nb2, out int maSomme, out double maMoyenne, out int min)
        {
             maSomme = MaSomme(nb1, nb2);
             maMoyenne = MaMoy(nb1, nb2);
            var tab = new int[] { nb1, nb2 };
            min = MonMin(tab);
        }
    }
}
