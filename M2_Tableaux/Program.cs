﻿using System;

class Program
{
    static void Main(string[] args)
    {
        #region "1D"
        #region Questions
        // 1. Déclarez moi un tableau à 1 dimension de 3 entiers avec comme valeurs 10, 15, 20 (2 méthodes)
        int[] monTab = new int[3];
        monTab[0] = 10;
        monTab[1] = 50;
        monTab[2] = 20;

        var _monTab = new int[] { 0, 5, 3 };

        // 2. Afficher le second élément du tableau créé précédemment
        Console.WriteLine(monTab[1]);

        // 3. Afficher la taille du tableau créé précédemment
        Console.WriteLine(monTab.Length);

        // 4. Parcourir le tableau créé précédemment et afficher les éléments (2 méthodes)
        for (int i = 0; i < monTab.Length; i++)
        {
            Console.WriteLine(monTab[i]);
        }
        
        foreach (var elements in monTab)
        {
            Console.WriteLine(elements);
        }

        #endregion
        #region Reponses
        // 1.
        int[] _tab1 = new int[3];
        _tab1[0] = 10;
        _tab1[1] = 15;
        _tab1[2] = 20;

        int[] _tab1b = { 10, 15, 20 };

        // 2.
        Console.WriteLine(_tab1[1]);

        // 3.
        Console.WriteLine(_tab1.Length);

        // 4.
        for (int _i = 0; _i < _tab1.Length; ++_i)
        {
            Console.WriteLine(_tab1[_i]);
        }

        foreach (var _element in _tab1)
        {
            Console.WriteLine(_element);
        }
        #endregion
        #endregion

        #region "n-D"
        #region Questions
        /* 1. Déclarez moi un tableau à 2 dimensions (matrice) avec 2 lignes et 3 colonnes, et les valeurs suivantes : (2 méthodes)
         * ________________
         * | 10 | 20 | 30 |
         * | 45 | 25 | 15 |
         * ----------------
         */
        int[,] maMatrice = new int[2, 3];
        maMatrice[0, 0] = 10;
        maMatrice[0, 1] = 20;
        maMatrice[0, 2] = 30;
        maMatrice[1, 0] = 45;
        maMatrice[1, 1] = 25;
        maMatrice[1, 2] = 15;

        int[,] maMatrice2 = new int[,] {{10, 20, 30 },{ 45, 25,15 }};

        // 2. Afficher le nombre de lignes du tableau créé précédemment
        Console.WriteLine(maMatrice.GetLength(0));

        // 3. Afficher le nombre de colonnes du tableau créé précédemment
        Console.WriteLine(maMatrice.GetLength(1));

        // 4. Parcourir le tableau créé précédemment et afficher les éléments
        for (int i = 0; i < maMatrice.GetLength(0); i++)
        {
            for (int j = 0; j < maMatrice.GetLength(1); j++)
            {
                Console.WriteLine(maMatrice[i, j]);
            }
        }

        #endregion
        #region Reponses
        // 1.
        int[,] _matrice = new int[2, 3];
        _matrice[0, 0] = 10;
        _matrice[0, 1] = 20;
        _matrice[0, 2] = 30;
        _matrice[1, 0] = 45;
        _matrice[1, 1] = 25;
        _matrice[1, 2] = 15;

        int[,] _matrice2 = { { 10, 20, 30 }, { 45, 25, 15 } };

        // 2.
        Console.WriteLine("Nombre de ligne du tableau 2D: " + _matrice2.GetLength(0));

        // 3.
        Console.WriteLine("Nombre de colonnes du tableau 2D: " + _matrice2.GetLength(1));

        // 4.
        for (int _ligne = 0; _ligne < _matrice2.GetLength(0); ++_ligne)
        {
            for (int _colonne = 0; _colonne < _matrice2.GetLength(1); ++_colonne)
            {
                Console.WriteLine(_matrice2[_ligne, _colonne]);
            }
        }
        #endregion
        #endregion
    }
}
