﻿// Ceci est un commentaire sur une ligne
/* Ceci
 * est
 * un
 * commentaire
 * sur
 * plusieurs
 * lignes
 */
using System; // Import de la bibiliothèque System

class Program
{
    static void Main(string[] args) // Point d'entrée du programme
    {
        Console.WriteLine("Bonjour le monde !"); // Ecrire("Bonjour le monde !")
        var lecture = Console.ReadLine(); // Lire(lecture)

        #region Variables
        #region Questions
        /* Déclaration explicite */
        // 1. Déclarez puis initialisez moi un entier avec comme valeur 15

        // 2. Déclarez puis initialisez moi un double avec comme valeur 10.5

        // 3. Déclarez puis initialisez moi une chaine de caractères avec comme valeur "J'aime le C#"

        // 4. Déclarez moi un rectangle puis initialisez sa largeur à 7.3

        /* Déclaration implicite */
        // 5. Déclarez moi un bool avec comme valeur false

        // 6. Déclarez moi une "pétale margeurite" avec comme valeur PetalesMargeurite.Passionnement

        /* Constantes */
        // 7. Déclarez moi une constante (caractère) avec comme valeur 'P'

        #endregion
        #region Reponses
        // 1.
        int monEntier;
        monEntier = 15;

        // 2.
        double monDouble = 10.5;

        // 3.
        string maChaine;
        maChaine = "J'aime le C#";

        // 4.
        Rectangle monRectangle;
        monRectangle.largeur = 7.3;

        // 5.
        var maVariable = false;

        // 6.
        var maPetale = PetalesMargeurite.Passionnement;

        // 7.
        const char MA_CONSTANTE = 'P';
        #endregion
        #endregion

        #region Operateurs
        int v1 = 5, v2 = 19;
        bool soif = true, faim = false;

        #region Questions
        // 1. Additioner v1 et v2
        Console.WriteLine(v1 + v2);

        // 2. Incrémenter v1
        v1++;
        // 3. Décrémenter v2 de 5
        v2 -= 5;
        // 4. Tester l'égalité entre v1 et v2 (doit retourner false)
        var test = v1 == v2;
        // 5. Afficher si j'ai faim ou si j'ai soif (TODO: à reformuler)
        Console.WriteLine(soif || faim);

        // 6. Afficher "Bonjour" et "Je suis en formation" sur 2 lignes, en 1 seule instruction (sans utliser l'opérateur de chaine verbatim)
        Console.WriteLine("Bonjour\nJe suis en formation");
        // 7. Afficher "Les variables v1 et v2 valent ... et ..." en utilisant l'interpolation de chaine (2 méthodes)
        Console.WriteLine("Les variables v1 et v2 valent {0} et {1}", v1, v2 );
        Console.WriteLine($"Les variables v1 et et v2 valent {v1} et {v2}");
        #endregion
        #region Reponses
        // 1.
        Console.WriteLine(v1 + v2);

        // 2.
        v1++;
        Console.WriteLine(v1);

        // 3.
        v2 -= 5;
        Console.WriteLine(v2);

        // 4.
        Console.WriteLine(v1 == v2);

        // 5.
        Console.WriteLine(soif || faim);

        // 6.
        Console.WriteLine("Bonjour\nJe suis en formation");

        // 7.
        Console.WriteLine("Les variables v1 et v2 valent {0} et {1}", v1, v2);
        Console.WriteLine($"Les variables v1 et v2 valent {v1} et {v2}");
        #endregion
        #endregion

        #region Transtypage
        byte monOctet = 5;
        int entierConversion = 10;
        string chaineConversion = "15";

        #region Questions
        /* Conversion implicite / explicite (cast) */
        // 1. Convertissez moi monOctet en entier
        int monOctetEnEntier = monOctet;
        /* ou */
        //var monOctetEnEntier = (int)monOctet;
        // 2. Convertissez moi entierConversion en octet
        var monEenO = (byte)entierConversion;
        /* Conversion avec la classe Convert */
        // 3. Convertissez moi entierConversion en octet
        var entierEnO = Convert.ToByte(entierConversion);
        // 4. Convertissez moi chaineConversion en entier
        var maChaineEnEntier = Convert.ToInt32(chaineConversion);
        // 5. Demandez puis affichez l'âge
        var age = int.Parse(Console.ReadLine());
        Console.WriteLine(age);
        #endregion
        #region Reponses
        // 1.
        int oe = monOctet;

        // 2.
        byte eo = (byte)entierConversion;

        // 3.
        byte eoc = Convert.ToByte(entierConversion);

        // 4.
        int cec = Convert.ToInt32(chaineConversion);

        // 5.
        Console.Write("Merci de fournir votre âge : ");
        int age2 = Convert.ToInt32(Console.ReadLine());
        //int age = int.Parse(Console.ReadLine());
        Console.WriteLine("Votre age est : " + age2);
        #endregion
        #endregion

        #region Conditions
        int nb = 5;
        int note = 10;
        string ValeurZero;

        #region Questions
        // 1. Tester si nb est positif, égal à zéro, ou négatif
        if (nb < 0)
        {
            ValeurZero = "nb est plus petit que zero";
        }
        else if (nb == 0)
        {
            ValeurZero = "nb est égal à 0";
        }
        else
        {
            ValeurZero = "nb est plus grand que 0";
        }

        // 2. Tester les différents cas pour note et afficher le résultat : 0 = "Recalé" ; 10, 11, 12 = "Juste la moyenne" ; Par défaut = "Réussite garantie"
        switch (note)
        {
            case 0:
                Console.WriteLine("Recalé");
                        break;
            case 10:
            case 11:
            case 12:
                Console.WriteLine("Juste la moyenne");
                break;
            default:
                Console.WriteLine("Réussite garantie");
                break;
        }

        // 3. Utiliser l'opérateur ternaire avec une variable int pour tester si 10 > 5, si oui : variable = 25 ; si non variable = 15
        int maVariabletern = 10;
        int monTest;
        monTest = (maVariabletern > 5)  ?  25  : 15;

        /* si j'avais voulu augmenter ma variable en fonction du resultat du test*/
        int maVariable2 = 10;
        int _monTest;
        maVariable2 = (maVariable2 > 5) ? maVariable2 + 15 : 15;

        #endregion
        #region Reponses
        // 1.
        if (nb > 0)
        {
            Console.WriteLine("nb est positif");
        }
        else if (nb == 0)
        {
            Console.WriteLine("nb est égal à zéro");
        }
        else
        {
            Console.WriteLine("nb est négatif");
        }

        // 2.
        switch (note)
        {
            case 0:
                Console.WriteLine("Recalé");
                break;
            case 10:
            case 11:
            case 12:
                Console.WriteLine("Juste la moyenne");
                break;
            default:
                Console.WriteLine("Réussite garantie");
                break;
        }

        // 3.
        int entierTernaire = (10 > 5) ? 25 : 15;
        #endregion
        #endregion

        #region Boucles

        #region Questions
        // 1. Répéter et afficher 5 fois "Passage n°i" avec i la valeur de l'index
        for (int i = 1; i <= 5; i++)
        {
            Console.WriteLine("Passage n°" + i);
        }
        // 2. Tant que x < 5, avec comme valeur de départ x = 1, répéter et afficher "Passage n°x"
        int x = 1; 
        while (x < 5)
        {
            Console.WriteLine("Passage n°" + x);
            x++;
        }
        // 3. Répéter et afficher "Passage n°x1", tant que x1 < 5, avec comme valeur de départ x = 1
        x = 1;
        do
        {
            Console.WriteLine("Passage n°" + x);
            x++;
        } while (x < 5);


        #endregion
        #region Reponses
        // 1.
        for (int i = 0; i < 5; ++i)
        {
            Console.WriteLine("Passage N°: " + i);
        }

        // 2.
        int _x = 1;
        while (_x < 5)
        {
            Console.WriteLine("Passage n°: " + x);
            ++_x;
        }

        // 3.
        int x1 = 1;
        do
        {
            Console.WriteLine("Passage n°: " + x1);
            ++x;
        } while (x < 5);
        #endregion
        #endregion
    }
}
