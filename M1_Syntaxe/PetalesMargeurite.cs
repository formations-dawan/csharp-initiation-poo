﻿enum PetalesMargeurite
{
    UnPeu = 1,
    Beaucoup,
    Passionnement,
    ALaFolie,
    PlusQueTout,
    PasDuTout
}